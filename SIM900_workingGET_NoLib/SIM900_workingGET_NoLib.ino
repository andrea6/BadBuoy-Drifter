#include <SoftwareSerial.h>
SoftwareSerial SIM900(7, 8);
 
void setup()
{
  Serial.begin(9600);
  SIM900.begin(19200);
  
  Serial.println("Starting up");
  
  SIM900.println("AT+CGATT");                                                        // AT command to send SMS message
  delay(100);
  toSerial();
  
  Serial.print(F("Setting connection type..."));
    // bearer settings
  SIM900.println("AT+SAPBR=3,1,\"CONTYPE\",\"GPRS\"");
  delay(1000);
  
  toSerial();

  // bearer settings
  Serial.print(F("Setting bearer settings..."));
  SIM900.println("AT+SAPBR=3,1,\"APN\",\"internet.emt.ee\"");
  delay(1000);
  toSerial();

  // bearer settings
  Serial.print(F("Setting up GPRS..."));
  SIM900.println("AT+SAPBR=1,1");
  delay(1000);
  toSerial();
  
  Serial.println(F("All systems are go."));

}
 

 
 
void loop()
{
 // initialize http service
   SIM900.println("AT+HTTPINIT");
   delay(2000); 
   toSerial();

   // set http param value
   
   SIM900.println("AT+HTTPPARA=\"URL\",\"http://www.mway.it/temp/send.php?payload=AUTOMATED!\"");
   delay(2000);
   toSerial();

   // set http action type 0 = GET, 1 = POST, 2 = HEAD
   SIM900.println("AT+HTTPACTION=0");
   delay(6000);
   toSerial();

   // read server response
   SIM900.println("AT+HTTPREAD"); 
   delay(1000);
   toSerial();

   SIM900.println("");
   SIM900.println("AT+HTTPTERM");
   toSerial();
   delay(300);

   SIM900.println("");
   delay(10000);
}

void toSerial()
{
  while(SIM900.available()!=0)
  {
    Serial.write(SIM900.read());
  }
}
