#include <SoftwareSerial.h>

#include <OneWire.h>
#include <Wire.h>
#include <DallasTemperature.h>

// #include <SD.h>
#include <Fat16.h>  

SdCard card;
static char filename[] = "log.txt";

#include "TinyGPS.h"


// Device configuration
#define CS_PIN_SD 10
#define ONE_WIRE_BUS 4

#define TRANSMISSION_INTERVAL 30000

// Defines the pin on which the OneWire devices (e.g. Temperaturesensor) will be attached
OneWire oneWire(ONE_WIRE_BUS);
// Initializes the Dallas temperature sensor
DallasTemperature onewire_sensors(&oneWire);
// Creates a Serial emulator for GPS comm
SoftwareSerial ss(8,9);
// Creates a NMEA parser with TinyGPS
TinyGPS gps;

SoftwareSerial SIM900(6, 7);

unsigned long fix_age, time, date, speed;
long lat = 0, lng = 0;
float temperature = 0;
boolean gps_data_ready = false;

unsigned long last_transmission;

void setup()
{ 
  // Initialize Serial Port. Should Leonardo be used, it needs a waiting loop also.
  Serial.begin(9600);

  // GPS MUST WORK at 9600bps!!! DO NOT CHANGE
  ss.begin(9600);
  // Initialize SD Card Data Logger
  initSD(CS_PIN_SD);
  initOneWire(ONE_WIRE_BUS);
  last_transmission = millis();

  pinMode(5,OUTPUT);

  tone(5,1000);
  delay(500);
  tone(5,2000);
  delay(500);
  tone(5,3000);
  delay(500);
  noTone(5);
  delay(1000);

}

void loop()
{
  onewire_sensors.requestTemperatures();
  temperature = onewire_sensors.getTempCByIndex(0);

  smartDelay(1000);
  readGPS();

  String s = pack_data();
  Serial.println(s);

  tone(5,2000);
  smartDelay(50);
  noTone(5);

  Fat16 file;
  file.open(filename, O_CREAT | O_APPEND | O_WRITE);  
  file.writeError = false;
  file.println(s);
  file.close();
  

  if (millis() > last_transmission + TRANSMISSION_INTERVAL) {
    last_transmission = millis();
    send_data_gsm(s);
    
  }

  
}


void send_data_gsm(String data) {

  tone(5,800);
  smartDelay(300);
  noTone(5);

  Serial.println(F("GSM"));
  String at = "AT+";

  ss.end();
  SIM900.begin(9600);
  smartDelay(1000);

  SIM900.println(at+"CGATT=1"); 
  delay(100);
  GSMtoSerial();
  
    // bearer settings
  SIM900.println(at+"SAPBR=3,1,\"CONTYPE\",\"GPRS\"");
  smartDelay(1000);
  
  GSMtoSerial();

  // bearer settings
  SIM900.println(at+"SAPBR=3,1,\"APN\",\"internet.emt.ee\"");
  smartDelay(1000);
  GSMtoSerial();

  // bearer settings
  SIM900.println(at+"SAPBR=1,1");
  smartDelay(1000);
  GSMtoSerial();

// Turns on the radio
  
  SIM900.println(at+"HTTPINIT");
   smartDelay(2000); 
   GSMtoSerial();

   // set http param value
   
   SIM900.println(at+"HTTPPARA=\"URL\",\"www.mway.it/temp/send.php?p="+data+"\"");
   smartDelay(2000);
   GSMtoSerial();

   // set http action type 0 = GET, 1 = POST, 2 = HEAD
   SIM900.println(at+"HTTPACTION=0");
   smartDelay(6000);
   GSMtoSerial();

   // read server response
   SIM900.println(at+"HTTPREAD"); 
   smartDelay(1000);
   GSMtoSerial();

   SIM900.println("");
   SIM900.println(at+"HTTPTERM");
   GSMtoSerial();
   smartDelay(300);

   SIM900.println("");

   SIM900.end();
   ss.begin(9600);
   
}


// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}


void readGPS() {
   gps.get_position(&lat, &lng, &fix_age);  
   gps.get_datetime(&date, &time, &fix_age);
   speed = gps.speed();
   
   if (fix_age == TinyGPS::GPS_INVALID_AGE) {
   gps_data_ready = false;
   }
  else {
    gps_data_ready = true;
    tone(5,3000);
    smartDelay(50);
    noTone(5);
    smartDelay(100);
  }
}

// Initialization routines

void GSMtoSerial()
{
  while(SIM900.available()!=0)
  {
    Serial.write(SIM900.read());
  }
}

void initSD(unsigned short int csPin) {
  Serial.print("SD..");
  verify(card.begin(csPin));
  verify(Fat16::init(&card));
  
}

void initOneWire(unsigned short int owPin) {
  Serial.print(F("1W.."));
  onewire_sensors.begin();
  success();
  Serial.print(F("Sens.."));
  Serial.println(onewire_sensors.getDeviceCount());
  
  // report parasite power requirements
  //Serial.print("[INIT] Parasite power is "); 
  //if (onewire_sensors.isParasitePowerMode()) Serial.println("ON");
  //else Serial.println("OFF");
}


// Service routines
void verify(boolean condition) {
 if(condition)
  success();
 else
  fatal(); 
}


void fatal(){
  Serial.println(F("ERR"));
  delay(100);
  exit(0);
}

void success(){
  Serial.println(F("OK"));
}

String pack_data() {

  String d = "";
  String comma = F(",");
  d+=String(temperature);
  d+=comma;
  if(gps_data_ready) {
    d+=String(lat);
    d+=comma;
    d+=String(lng);
    d+=comma;
    d+=String(date);
    d+=comma;
    d+=String(time);
    d+=comma;
    d+=String(speed);

  }
  else {
    d=d+comma+comma+comma+comma;
  }
  return d;
  
}



