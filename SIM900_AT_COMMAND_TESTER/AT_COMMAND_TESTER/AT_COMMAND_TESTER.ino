/*
 * This program allows for writing to an AT device through arduino and software serial
 * Connections:
 * 
 * SIM900l Pin8 -> arduino Pin 8
 * SIM900l Pin7 -> arduino Pin 7
 * 
 * User * to send a 0x1A
 */

#include <SoftwareSerial.h>

SoftwareSerial Serial1(7, 8); // RX, TX

char endchar[2];


void setup() {
  endchar[0]='0x1A';
  endchar[1]='\0';
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }


  Serial.println("AT COMMAND TESTER (*=0x1A");

  // set the data rate for the SoftwareSerial port
  Serial1.begin(19200);

}

void loop() { // run over and over
  if (Serial1.available()) {
      Serial.write( Serial1.read());
  }
  
  
  if (Serial.available()) {
    char received = Serial.read();
    if (received == '*') {
      Serial.println("ENDLINE DETECTED, SENDING 0x1A instead");
      Serial1.write(0x1A);
    }
    else {
      Serial1.write(received);
    }
    
      
  }
}

